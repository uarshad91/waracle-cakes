export default class CakesService {

    constructor($http) {
        this.http = $http;
    }

    getCakes() {
        return this.http.get('http://52.31.91.48:5000/api/cakes').then(result => result.data);
    }

    addCake(name, comment, yumFactor) {

        var payload = {
            id: Date.now(),
            name: name,
            comment: comment,
            imageUrl: 'N/A',
            yumFactor: yumFactor
        }

        return this.http.post('http://52.31.91.48:5000/api/cakes', payload).then(result => result.status);
    }
}
export default class StateService {

    constructor() {
        this.data = {};
    }

    set(name, value) {
        //console.log('setting ' + name + ' with value ' + value);
        this.data[name] = value;
    }

    get(name) {
        //console.log('getting ' + name + ' with value ' + this.data[name]);
        return this.data[name];
    }

    getAll() {
        return this.data;
    }
}
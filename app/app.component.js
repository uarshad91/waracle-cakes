import appTemplate from './app.component.html';

class AppComponent {

    constructor() {
    }

}

export default {
    template: appTemplate,
    controller: AppComponent,
    controllerAs: 'ctrl'
}
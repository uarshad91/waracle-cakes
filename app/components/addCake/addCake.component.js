import addCakeTemplate from './addCake.component.html';

class AddCake {

    constructor(stateService, cakesService) {
        this.state = stateService;
        this.cakesService = cakesService;
        this.addCakes = false;
        this.yumminessLevels = [1, 2, 3, 4, 5];
        this.name = '';
        this.comment = '';
        this.showAlert = false;
    }

    get addCakes() {
        return this.state.get('addCakes');
    }

    set addCakes(value) {
        return this.state.set('addCakes', value);
    }

    addNewCake() {
        if (this.name !== '' && this.comment !== '' && this.yumFactor) {
            this.cakesService.addCake(this.name, this.comment, this.yumFactor);
            this.addCakes = false;
            this.state.set('viewCakes', true);
            this.yumFactor = undefined;
        } else {
            this.showAlert = true;
        }
    }

    dismissModal() {
        this.addCakes = false;
        this.state.set('viewCakes', true);
    }

    dismissAlert() {
        this.showAlert = false;
    }

    selectYumFactor(yumminess) {
        this.yumFactor = yumminess;
    }


}

export default {
    template: addCakeTemplate,
    controller: AddCake,
    bindings: {
        name: '<?',
        comment: '<?'
    }
}
import cakeListTemplate from './cakeList.component.html';

class CakeList {

    constructor(cakesService, stateService) {
        cakesService.getCakes().then(result => {
            this.cakes = result;
        });
        this.state = stateService;
    }

    set cakes(value) {
        this.state.set('cakes', value);
    }

    get cakes() {
        return this.state.get('cakes');
    }

    setCake(cake) {
        this.state.set('cake', cake);
    }

}

export default {
    template: cakeListTemplate,
    controller: CakeList
}
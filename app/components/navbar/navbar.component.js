import navbarTemplate from './navbar.component.html';

class Navbar {

    constructor(stateService) {
        this.state = stateService;
        this.viewCakes = true;
        this.addCakes = false;
    }

    get viewCakes() {
        return this.state.get('viewCakes');
    }

    get addCakes() {
        return this.state.get('addCakes');
    }

    set viewCakes(value) {
        this.state.set('viewCakes', value);
    }

    set addCakes(value) {
        this.state.set('addCakes', value);
    }

    activateCakeView() {
        this.viewCakes = true;
        this.addCakes = false;
    }

    activateAddCakeView() {
        this.viewCakes = false;
        this.addCakes = true;
    }

}

export default {
    template: navbarTemplate,
    controller: Navbar,
    controllerAs: 'ctrl'
}


import cakeInfoTemplate from './cakeInfo.component.html';

class CakeInfo {

    constructor(stateService) {
        this.state = stateService;
    }

    get cake() {
        return this.state.get('cake');
    }

    set cake(value) {
        this.state.set('cake', value);
    }

    dismissModal() {
        this.cake = undefined;
    }
}

export default {
    template: cakeInfoTemplate,
    controller: CakeInfo
}
# README #

This project aims to demonstrate my abilities with modern web development tools and technologies.

### Quick-start ###


* git clone https://uarshad91@bitbucket.org/uarshad91/waracle-cakes.git
* cd ./waracle-cakes
* npm start

### Tech Stack ###

* AngularJS
* Webpack
* Babel
* NPM
* Karma

### Reasoning ###

* **Why AngularJS?** I felt that through employing AngularJS component based architecture I could best demonstrate my abilities to work with both Angular and AngularJS. The component based architecture i've employed directly aligns with the 'Angular' approach and allows for easy migration to Angular (v2/4). Additionally, this approach promotes isolated well defined components with single responsibilities. Furthermore, I believe this approach allows for easy maintainability and extension.
* **Where are the test?!** Unfortunately, I was unable to spend as much time as I would have liked on this project and thus due to time constraints I had to submit what I had. Given that my schedule wasn't so busy I would have used Karma and Jasmine to carry out my testing.
* **... Inline styling?** Although I had originally planned to utilise dedicated style sheets, again, due to time constraints I decided to to drop the styling and go for basic inline styling.
* **Why Bulma?** I very specifically wanted to make use of Bulma in this project as it is very light weight and allows for easy responsive styling of your application. Where time allowed it I tried to utilise the responsive classes that Bulma exposes for a better mobile experience.

### Retrospective ###

* **Timing** In retrospect I feel I should have asked to take the test at a different time period (say a week or two later) *due to my busy schedule.
* **Deployment** I have deployed the application using Heroku however, I never took into account the fact that the cakes API serves content without SSL and thus my requests to the API are blocked as the application is served over HTTPS.
* **Responsiveness** If I could revisit this project, once the functionality was down I would have spent more time on styling with an emphasis on mobile responsiveness through Bulma.

### Foot Note ###
I thoroughly enjoyed this test as it really makes you stop and think as you have to build from scratch with consideration on tech stack and architecture, whereas with tests I have sat in the past the tech stack is dictated to you.

### Contact ###
uarshad91@gmail.com
usman.arshad@nazr.co.uk
(+44)07539218797
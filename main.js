import angular from 'angular';
import AppComponent from './app/app.component';
import CakeListComponent from './app/components/cakeList/cakeList.component';
import AddCakeComponent from './app/components/addCake/addCake.component';
import CakeInfoComponent from './app/components/cakeInfo/cakeInfo.component';
import NavbarComponent from './app/components/navbar/navbar.component';
import CakesService from './app/services/cakes.service';
import StateService from './app/services/state.service';

CakesService.$inject = ['$http'];

angular.module('app', [
])
.factory('cakesService', ($http) => new CakesService($http))
.factory('stateService', () => new StateService())
.component('appComponent', AppComponent)
.component('navbarComponent', NavbarComponent)
.component('cakeListComponent', CakeListComponent)
.component('addCakeComponent', AddCakeComponent)
.component('cakeInfoComponent', CakeInfoComponent);
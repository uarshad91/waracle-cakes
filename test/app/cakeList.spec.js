describe('cakeListComponent', function () {

    beforeEach(module('app'));

    beforeEach(inject(function ($injector) {
        $componentController = $injector.get('$componentController');
        controller = $componentController('cakeInfoComponent', {});
    }));

    it('should be empty', function () {
        expect(controller.count).toEqual(0);
    })

});